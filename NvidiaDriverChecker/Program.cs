﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Windows.Forms;

namespace NvidiaDriverChecker
{
    class Program
    {
        const string requestUri = "https://gfwsl.geforce.com/services_toolkit/services/com/nvidia/services/AjaxDriverService.php?func=DriverManualLookup&psid=101&pfid=817&osID=57&languageCode=1078&beta=null&isWHQL=1&dltype=-1&sort1=0&numberOfResults=1";
        private static HttpClient httpClient;
        private static bool shutDown = true;
        private delegate void EventHandler();
        private static event EventHandler DownloadDone;
        private static DateTime lastUpdate = DateTime.Now;
        private static long lastBytes = 0;
        private static double downloadSpeed = 0;
        private static long sinceLastTick = 0;
        private static int ProgressPercentage = 0;
        private static readonly object throttleLock = new object();

        [STAThread]
        static void Main(string[] args)
        {
            httpClient = new HttpClient();

            string latestDriverVersion = GetLatestDriverVersion(httpClient);
            int[] localGpuDriverVersionArray = GetGpuDriverVersion();
            string localGpuDriverVersion = $"{localGpuDriverVersionArray[0]}.{localGpuDriverVersionArray[1]}";

            Console.WriteLine($"Current driver version: {localGpuDriverVersion}.\nLatest driver version: {latestDriverVersion}.");

            if (NewerVersionAvailable(latestDriverVersion, localGpuDriverVersion))
            {
                Console.WriteLine("Driver needs to be updated.");
                string gpuDriverDownloadUri = GetLatestDriverDownloadUri(httpClient);
                Console.WriteLine("Driver download link: " + gpuDriverDownloadUri);

                Console.WriteLine("Press \"Y\" to download the driver or push any other key to exit.");
                string userInput = Console.ReadKey().KeyChar.ToString();
                if (string.Equals(userInput, "y", StringComparison.OrdinalIgnoreCase))
                {
                    ClearCurrentConsoleLine();
                    shutDown = false;
                    DownloadDone += new EventHandler(OnDownloadDone);
                    Console.WriteLine();
                    Console.WriteLine("Downloading GPU driver:");
                    string directory = GetDirectory();
                    if (directory == null)
                    {
                        Console.WriteLine("Press any key to exit.");
                        Console.ReadKey();
                        return;
                    }
                    string filePath = directory + $"NVidia Driver {latestDriverVersion}.exe";
                    DownloadFile(gpuDriverDownloadUri, filePath);
                }
            }
            else
            {
                Console.WriteLine("Driver is up to date.");
                Console.ReadKey();
            }

            if (shutDown)
            {
                return;
            }
            while (!shutDown)
            {
                Thread.Sleep(100);
            }

            ClearUserInputs();
            Console.WriteLine("\nDownload is done. Press any key to exit.");
            Console.ReadKey();
        }

        private static void ClearUserInputs()
        {
            if (Console.KeyAvailable)
            {
                Console.ReadKey(false);
            }
        }

        private static void OnDownloadDone()
        {
            shutDown = true;
        }

        private static int[] GetGpuDriverVersion()
        {
            string driverVersion = NvAPIWrapper.NVIDIA.DriverVersion.ToString();
            int majorVersion = Convert.ToInt32(driverVersion.Substring(0, 3));
            int minorVersion = Convert.ToInt32(driverVersion.Substring(3, 2));
            return new int[] { majorVersion, minorVersion };
        }

        private static string GetLatestDriverVersion(HttpClient httpClient)
        {
            HttpResponseMessage driverQueryResponse = httpClient.GetAsync(requestUri).Result;
            string driverQueryContent = driverQueryResponse.Content.ReadAsStringAsync().Result;
            dynamic driverQueryContentObject = JsonConvert.DeserializeObject<dynamic>(driverQueryContent);

            return driverQueryContentObject?.IDS[0]?.downloadInfo?.Version;
        }

        private static string GetLatestDriverDownloadUri(HttpClient httpClient)
        {
            HttpResponseMessage driverQueryResponse = httpClient.GetAsync(requestUri).Result;
            string driverQueryContent = driverQueryResponse.Content.ReadAsStringAsync().Result;
            dynamic driverQueryContentObject = JsonConvert.DeserializeObject<dynamic>(driverQueryContent);

            return driverQueryContentObject?.IDS[0]?.downloadInfo?.DownloadURL;
        }

        private static bool NewerVersionAvailable(string latestDriverVersion, string gpuDriverVersion)
        {
            int localMajor = Convert.ToInt32(gpuDriverVersion.Split('.')[0]);
            int localMinor = Convert.ToInt32(gpuDriverVersion.Split('.')[1]);

            int latestMajor = Convert.ToInt32(latestDriverVersion.Split('.')[0]);
            int latestMinor = Convert.ToInt32(latestDriverVersion.Split('.')[1]);

            if (latestMajor > localMajor)
            {
                return true;
            }
            else if (latestMajor == localMajor)
            {
                if (latestMinor > localMinor)
                {
                    return true;
                }
            }

            return false;
        }

        private static void DownloadFile(string Uri, string filePath)
        {
            using (WebClient webClient = new WebClient())
            {
                webClient.DownloadProgressChanged += WebClient_DownloadProgressChanged;
                webClient.DownloadFileAsync(new Uri(Uri), filePath);
                Console.WriteLine($"Saving file: {filePath}");
            }

            Thread downloadHandler = new Thread(HandleDownload);
            downloadHandler.Start();
        }

        private static string GetDirectory()
        {
            string folderPath = null;
            using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
            {
                if (LastSavedDirectory != null)
                    folderBrowserDialog.SelectedPath = LastSavedDirectory;

                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    folderPath += folderBrowserDialog.SelectedPath;
                    if (!folderPath.EndsWith(@"\"))
                        folderPath += @"\";
                    LastSavedDirectory = folderPath;
                }
            }
            return folderPath;
        }

        private static void WebClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            lock (throttleLock)
            {
                ProgressPercentage = e.ProgressPercentage;

                long bytesChange = e.BytesReceived - sinceLastTick;
                sinceLastTick = e.BytesReceived;
                lastBytes += bytesChange;
            }
        }

        private static void HandleDownload()
        {
            while (!shutDown)
            {
                if (ProgressPercentage == 100)
                {
                    ClearCurrentConsoleLine();
                    Console.Write("File transfer: " + ProgressPercentage + "%.");
                    DownloadDone();
                    return;
                }

                TimeSpan timeSpan = DateTime.Now - lastUpdate;
                downloadSpeed = ProgressPercentage != 100 ? Math.Round(lastBytes / 1000 / 1000 / timeSpan.TotalSeconds, 2) : 0;
                lastBytes = 0;
                lastUpdate = DateTime.Now;
                ClearCurrentConsoleLine();
                Console.Write("File transfer: " + ProgressPercentage + "%. Speed: " + downloadSpeed + " MB/s");
                Thread.Sleep(1000);
            }
        }

        private static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }

        private static string LastSavedDirectory
        {
            get
            {
                System.Collections.Specialized.NameValueCollection appSettings = ConfigurationManager.AppSettings;
                return appSettings["LastSavedDirectory"];
            }
            set
            {
                Configuration configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                KeyValueConfigurationCollection settings = configFile.AppSettings.Settings;
                if (settings["LastSavedDirectory"] == null)
                {
                    settings.Add("LastSavedDirectory", value);
                }
                else
                {
                    settings["LastSavedDirectory"].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
        }

        #region Deprecated
        private static bool NewerVersionAvailable(DateTime latestDriverVersion, DateTime gpuDriverVersion)
        {
            return DateTime.Compare(latestDriverVersion, gpuDriverVersion) > 0;
        }

        private static DateTime GetLatestDriverDate(HttpClient httpClient)
        {
            const string requestUri = "https://gfwsl.geforce.com/services_toolkit/services/com/nvidia/services/AjaxDriverService.php?func=DriverManualLookup&psid=101&pfid=817&osID=57&languageCode=1078&beta=null&isWHQL=0&dltype=-1&sort1=0&numberOfResults=1";
            HttpResponseMessage driverQueryResponse = httpClient.GetAsync(requestUri).Result;
            string driverQueryContent = driverQueryResponse.Content.ReadAsStringAsync().Result;
            dynamic driverQueryContentObject = JsonConvert.DeserializeObject<dynamic>(driverQueryContent);

            string releaseDate = driverQueryContentObject?.IDS[0]?.downloadInfo?.ReleaseDateTime;
            string releaseDateMonthString = new string(releaseDate.Where(char.IsLetter).ToArray());
            int releaseDateMonth = DateTime.ParseExact(releaseDateMonthString, "MMMM", CultureInfo.CurrentCulture).Month;
            string releaseDateDayAndYear = new String(releaseDate.Where(char.IsDigit).ToArray());
            int releaseDateDay = Convert.ToInt32(releaseDateDayAndYear.Substring(0, 2));
            int releaseDateYear = Convert.ToInt32(releaseDateDayAndYear.Substring(2, 4));

            return new DateTime(releaseDateYear, releaseDateMonth, releaseDateDay);
        }

        private static DateTime GetLocalGpuDriverDate()
        {
            ManagementObjectSearcher gpuInformation = new ManagementObjectSearcher("select * from Win32_VideoController");
            IList<ManagementObject> gpuInformationCollection = new List<ManagementObject>();
            foreach (ManagementObject managementObject in gpuInformation.Get())
            {
                gpuInformationCollection.Add(managementObject);
            }

            string driverDate = gpuInformationCollection.First()["DriverDate"].ToString().Substring(0, 8);
            return new DateTime(Convert.ToInt32(driverDate.Substring(0, 4)), Convert.ToInt32(driverDate.Substring(4, 2)), Convert.ToInt32(driverDate.Substring(6, 2)));
        }
        #endregion
    }
}
